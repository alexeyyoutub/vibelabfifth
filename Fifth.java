package Fifth;

import java.util.Scanner;

public class Fifth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
      //  String str = str1;
        boolean check = false;
        int count_oop = 0, i = 0;
        String[] words = str.split(" ");
        for (i = 0; i < words.length - 1; i++){
            if (words[i+1].endsWith(".")) {
                check = true;
                words[i+1] = words[i+1].substring(0, words[i+1].length()-1);
            }
            if ((words[i].equals("Object-oriented") && words[i+1].equals("programming")) ||
                    (words[i].equals("object-oriented") && words[i+1].equals("programming")))
            {
                count_oop++;
                if (count_oop % 2 == 0) {
                    words[i] = "OO";
                    words[i+1] = "P";
                }
            }
            if (check) {
                words[i+1] += '.';
                check = false;
            }
        }
        str = "";
        for (i = 0; i < words.length; i++){
            str += words[i] + ' ';
        }
        str = str.replace("OO P", "OOP");
        System.out.println("The line with the change to OOP:");
        System.out.println(str);

        int symb;
        String excess = "";
        for (i = 0; i < str.length(); i++){
            symb = (int)(str.charAt(i));
            excess += str.charAt(i);
            if (symb > 32 && symb != 45 && symb < 47)
                str = str.replace(excess, "");
            excess = "";
        }
        words = str.split(" ");
        String wcheck = "", min = words[0], word;
        for (int j = 0; j < min.length(); j++) {
            if (min.lastIndexOf(min.charAt(j)) == j)
                wcheck += min.charAt(j);
        }
        String mincheck = wcheck;
        wcheck = "";
        for (i = 0; i < words.length; i++)
        {
            word = words[i];
            for (int j = 0; j < word.length(); j++){
                if (word.lastIndexOf(word.charAt(j)) == j)
                    wcheck += word.charAt(j);
            }
            if (wcheck.length() < mincheck.length())
            {
                min = word;
                mincheck = wcheck;
            }
            wcheck = "";
        }
        System.out.println("A word in which the number of different characters is minimal: " + min);

        int num, count_en = 0;
        check = false;
        for (i = 0; i < words.length; i++) {
            word = words[i];
            for (int j = 0; j < word.length(); j++){
                num = (int)(word.charAt(j));
                if ((num > 64 && num < 91) || (num > 96 && num < 123))
                    check = true;
                else
                    check = false;
            }
            if (check)
                count_en++;
        }
        System.out.println("The number of words containing only Latin alphabet characters: " + count_en);

        check = false;
        String palindroms = "";
        for (i = 0; i < words.length; i++) {
            word = words[i];
            for (int j = 0; j < word.length() / 2; j++){
                num = word.length()-1-j;
                if (word.charAt(j) != word.charAt(num))
                {
                    check = false;
                    break;
                }
                else
                    check = true;
            }
            if (check)
            {
                palindroms += word + " ";
                check = false;
            }
        }
        if (palindroms.equals("")) palindroms = "_";
        System.out.println("Palindrome words: " + palindroms);
    }
}